# Duniter technical description

Rédaction d'une description technique pour le projet Duniter.
L'objectif est de produire un document donnant une explication quasi-complète de la théorie qui sous-tend le protocole Duniter.

L'auteur, Matograine, a rassemblé des sources déjà rédigées par les autres auteurs.
Il connaît assez bien le protocole Duniter, mais n'a pas participé à son élaboration.
Il n'est pas assez compétent en théorie des graphes ni en cryptographie pour entrer dans les détails du projet.
Cependant, il l'est assez pour rédiger la majeure partie du texte explicatif, et invite qui le souhaite à l'accompagner dans cette rédaction.

## Installation et compilation sur système Debian

Les sources sont rédigées en syntaxe RestructuredText et compilées avec Sphinx.

Pour compiler les sources markdown vers les versions html, ebook et pdf vous allez avoir besoin de ces dépendances:

```bash
sudo apt install python3-pip latexmk make texlive-latex-extra texlive-latex-recommended texlive-fonts-recommended
python3 -m pip install sphinx==3.2.1 --user
bash ./build.sh
```

## Modifications

Les modifications au contenu doivent être faites dans le dossier `sources`, et dans le dossier correspondant à la langue voulue.
Le script build.sh permet de générer les documents en local.
Les documents sont automatiquement générés par la CI lors de toute modification, stockés temporairement, et effacés par toute nouvelle version.

## Traductions

Le Whitepaper est volontairement rédigé en anglais à l'origine.
Les traductions ne sont pas souhaitées tant que le contenu n'est pas arrété (i.e. tant qu'une V1 n'est pas publiée).

## Publication

La publication se fait en plusieurs temps :

```
bash publish.sh <tag number X.X.X>
git push --tags origin <branch>
```

Faire une demande de MR vers la branche `master`. Valider manuellement le pipeline lorsque la MR a été acceptée.
La publication est limitée aux membres du projet. 

### Références

D'autres whitepapers, pour observer le ton et les sujets à aborder.
* Bitcoin : https://bitcoin.org/bitcoin.pdf
* Ethereum : https://github.com/ethereum/wiki/wiki/White-Paper
* Nano : https://content.nano.org/whitepaper/Nano_Whitepaper_en.pdf

### Forme :

* Scientifique : sources en bas (BTC met les numéros de renvois en face des sources)
* liens internes vers graphiques


