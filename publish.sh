#!/bin/bash

# use : bash ./publish.sh <VERSION>

# This script:
# * publishes a version of the WP
# * creates a tag
# Once you used it, publish with git push --tags

VERSION=$1

check_argument_specified() {
	if [[ -z $VERSION ]]; then
		error_message "You should specify a version number as argument"
	fi
}

check_version_format() {
	if [[ ! $VERSION =~ ^[0-9]+.[0-9]+.[0-9]+[0-9A-Za-z]*$ ]]; then
		error_message "Wrong format version"
	fi
}

update_version() {
	sed -i "s/release = '*.*'/release = '$VERSION'/" source/conf.py
	git diff
}

commit_tag() {
        git add ./source/conf.py
	git commit ./source/conf.py -m "version $VERSION"
	git tag "$VERSION" -a -m "$VERSION"
}

error_message() {
	echo $1
	exit
}


check_argument_specified
check_version_format
update_version
commit_tag
