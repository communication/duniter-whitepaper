#!/bin/bash

# Ce script sert à publier le WhitePaper.
# La première version crée les documents whitepaper.md et whitepaper.html.
# Le doc Markdown complet est volontairement supprimé. Je préfère que les modifications soient faites sur les différentes parties.
# ce script doit être lancé dans le dossier contenant les chapitres.

# dépendances : sphinx-build
hash sphinx-build 2>/dev/null || { echo >&2 "Ce générateur a besoin de la commande sphinx-build pour fonctionner. Mais cette commande n'est pas installée. Fin de l'exécution."; exit 1; }
#hash pandoc 2>/dev/null || { echo >&2 "Ce générateur peut utiliser ebook-convert fourni avec Calibre, disponible dans les dépots apt. Mais cette commande n'est pas installée. Fin de l'exécution."; exit 1; }

# define files
index_rst="source/index.rst"
index_pdf="source/index.rst.pdf"
index_old="source/index.rst.OLD"
base_index="source/base_index.rst"

build_dir="./build"


# clean build
rm -rf build/*

function clean_source {

if [ -e $index ] ; then
   rm $index_rst
   rm $index_old
   rm $index_pdf
fi

}

## Compilation HTML des différents chapitres
### (ceci devrait être refactorisé pour le support de davantage de langues)
### version anglaise

function generate_lang () {

lang=$1

clean_source

cp $base_index $index_rst
cp $base_index $index_pdf
for i in source/parts/$lang/*.rst ; do
    cat $i >> $index_rst

    # PDF : remove the "source" section title
    if [[ ! $i =~ 9_sources.rst ]] ; then
        cat $i >> $index_pdf
    else
        sed '1,5d' $i >> $index_pdf
    fi

done

# HTML compilation

mkdir $build_dir/$lang
mkdir $build_dir/$lang/html/

sphinx-build -b html source $build_dir/$lang/html/

if [ -e $build_dir/$lang/html/index.html ] ; then
    echo "===== HTML OK ====="
fi

## Compilation PDF
mv $index_rst $index_old
mv -f $index_pdf $index_rst

## build pdf
sphinx-build -M latexpdf source $build_dir/$lang

# build ebooks
#ebook-convert $build_dir/$lang/index.html $build_dir/whitepaper_$lang.epub

# move assets for publication

## create html archive in build dir
cd $build_dir/$lang/html/
tar -czf ../../html_$lang.gz ./*
cd ../../..

# move pdf in build dir
mv $build_dir/$lang/latex/dunitertechnicaldescription.pdf $build_dir/whitepaper_$lang.pdf

}

# we should build the doc once the translation is done
generate_lang en
