# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Duniter technical description'
copyright = '2020, Matograine'
author = 'Elois, Cgeek, Inso, Matograine'

# The full version, including alpha/beta/rc tags
release = '0.0.4'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
'sphinx.ext.mathjax',
]

mathjax_path = './js/tex-mml-chtml.js'
html_static_path = ['_static']
# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['parts/*', 'base_index.rst']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#

html_theme = 'alabaster'
html_title = 'Duniter technical description'

html_sidebars = {
    '**': [
        'localtoc.html',
    ]
}

html_theme_options = {
    'sidebar_width': '400px',
    'page_width': '1200px',
    'fixed_sidebar': False,
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".

# -- Options for LATEX/PDF output -------------------------------------------------
latex_elements = {
'papersize': 'a4paper',
'fncychap': '\\usepackage[Sonny]{fncychap}',
'extraclassoptions': 'openany,oneside'
}
latex_show_pagerefs = True
