
.. _sources:

Sources
-------

.. [#Bitcoin]
   Bitcoin Whitepaper, S.Nakamoto, 2008:
   `bitcoin.org/bitcoin.pdf <https://bitcoin.org/bitcoin.pdf>`__

.. [#RTM]
   Relative Theory of Money, S.Laborde, 2010:
   `en.trm.creationmonetaire.info/ <http://en.trm.creationmonetaire.info/>`__

.. [#OpenPGP]
   OpenPGP protocol defines standard formats for encrypted messages,
   signatures, private keys, and certificates for exchanging public
   keys. The GNU Privacy Handbook, M.Ashley, 1999:
   `www.gnupg.org/gph/en/manual.html#AEN335 <https://www.gnupg.org/gph/en/manual.html#AEN335>`__

.. [#EC]
   High-speed high-security signatures, D.J.Bernstein, N.Duif, T.Lange,
   P.Schwabe, B-Y.Yang. Journal of Cryptographic Engineering 2 (2012),
   77–89.
   `ed25519.cr.yp.to/ed25519-20110926.pdf <https://ed25519.cr.yp.to/ed25519-20110926.pdf>`__.

.. [#PPCoin]
   PPCoin: Peer-to-Peer Crypto-Currency with Proof-of-Stake, S.King &
   S.Nadal, 2012:
   `archive.org/details/PPCoinPaper <https://archive.org/details/PPCoinPaper>`__

.. [#Lightning]
   The Bitcoin Lightning Network, J.Poon & T.Dryja, 2016:
   `lightning.network/lightning-network-paper.pdf <http://lightning.network/lightning-network-paper.pdf>`__

.. [#CouchSurf]
   Surfing a Web of Trust, Reputation and Reciprocity on
   CouchSurfing.com, D.Lauterbach, H.Truong, T.Shah, L.Adamic:
   `snap.stanford.edu/class/cs224w-readings/lauterbach09trust.pdf <http://snap.stanford.edu/class/cs224w-readings/lauterbach09trust.pdf>`__

.. [#GnuPG]
   Public key validation on GnuPG manual, M.Ashley, 1999:
   `www.gnupg.org/gph/en/manual.html#AEN335 <https://www.gnupg.org/gph/en/manual.html#AEN335>`__

.. [#Sybil]
   The Sibyl Attack, J.R.Douceur:
   `www.microsoft.com/en-us/research/wp-content/uploads/2002/01/IPTPS2002.pdf <https://www.microsoft.com/en-us/research/wp-content/uploads/2002/01/IPTPS2002.pdf>`__

.. [#Dunbar]
   Neocortex size as a constraint on group size in primates,
   R.I.M.Dunbar, Journal of Human Evolution, 1992

.. [#Pareto]
   Pareto principle:
   `en.wikipedia.org/wiki/Pareto_principle <https://en.wikipedia.org/wiki/Pareto_principle>`__
